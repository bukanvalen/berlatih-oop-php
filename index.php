<?php
require ("animal.php");
require ("frog.php");
require ("ape.php");

//release 0
$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br><br>"; // false

//release 1
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->legs . "<br>"; // 2
echo $sungokong->cold_blooded . "<br>"; // false
$sungokong->yell(); // "Auooo"

echo "<br><br>" ;

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->legs . "<br>"; // 4
echo $kodok->cold_blooded . "<br>"; // true
$kodok->jump(); // "hop hop"
?>